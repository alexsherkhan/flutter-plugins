# client_wrapper_demo

To familiarize yourself with the **Client Wrapper** interface, a demo plugin with an example application was developed.

![preview](data/preview.png)

## Features

- `PluginRegistrar` - registering a platform-dependent plugin for subsequent work with it from Dart.
- `MethodChannel` - registration and operations with platform-dependent plugin methods from Dart code.
- `EventChannel` - registration, signature in Dart for events sent from the C++ part of the plugin.
- `TextureRegistrar` - registering a texture and drawing a pixel buffer on it.
- `BinaryMessenger` is a low-level component lying in the data exchange database in Flutter Embedder.
- `EncodableValue` - a component involved in data exchange between plugin components and Flutter.

[![button_docs](../../data/button_docs.png)](https://omprussia.gitlab.io/flutter/flutter/structure/plugins/#client-wrapper)
