# ffi_demo

An example of a platform-dependent plugin that works with the Aurora OS via the FFI.

![preview](data/preview.png)

Detailed information about the plugin can be found in the documentation:

[![button_docs](../../data/button_docs.png)](https://omprussia.gitlab.io/flutter/flutter/examples/ffi/)
