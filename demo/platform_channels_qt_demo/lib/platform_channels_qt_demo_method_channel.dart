// SPDX-FileCopyrightText: Copyright 2024 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause
import 'package:flutter/services.dart';

import 'platform_channels_qt_demo_platform_interface.dart';

// Platform plugin keys channels
const channelEvent = "platform_channels_qt_demo";

/// An implementation of [PlatformChannelsQtDemoPlatform] that uses method channels.
class MethodChannelPlatformChannelsQtDemo
    extends PlatformChannelsQtDemoPlatform {
  /// The method channel used to interact with the native platform.
  final eventChannel = const EventChannel(channelEvent);

  /// Scream network state with EventChannel
  @override
  Stream<bool?> stateNetworkConnect() {
    return eventChannel.receiveBroadcastStream().map((event) => event as bool);
  }
}
