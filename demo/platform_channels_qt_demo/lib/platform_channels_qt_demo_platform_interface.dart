// SPDX-FileCopyrightText: Copyright 2024 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause
import 'package:plugin_platform_interface/plugin_platform_interface.dart';

import 'platform_channels_qt_demo_method_channel.dart';

abstract class PlatformChannelsQtDemoPlatform extends PlatformInterface {
  /// Constructs a PlatformChannelsQtDemoPlatform.
  PlatformChannelsQtDemoPlatform() : super(token: _token);

  static final Object _token = Object();

  static PlatformChannelsQtDemoPlatform _instance = MethodChannelPlatformChannelsQtDemo();

  /// The default instance of [PlatformChannelsQtDemoPlatform] to use.
  ///
  /// Defaults to [MethodChannelPlatformChannelsQtDemo].
  static PlatformChannelsQtDemoPlatform get instance => _instance;

  /// Platform-specific implementations should set this with their own
  /// platform-specific class that extends [PlatformChannelsQtDemoPlatform] when
  /// they register themselves.
  static set instance(PlatformChannelsQtDemoPlatform instance) {
    PlatformInterface.verifyToken(instance, _token);
    _instance = instance;
  }

  Stream<bool?> stateNetworkConnect() {
    throw UnimplementedError('connectNetworkState() has not been implemented.');
  }
}
