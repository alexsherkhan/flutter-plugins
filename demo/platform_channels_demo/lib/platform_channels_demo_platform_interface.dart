// SPDX-FileCopyrightText: Copyright 2024 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause
import 'package:plugin_platform_interface/plugin_platform_interface.dart';

import 'platform_channels_demo_method_channel.dart';

abstract class PlatformChannelsDemoPlatform extends PlatformInterface {
  /// Constructs a PlatformChannelsDemoPlatform.
  PlatformChannelsDemoPlatform() : super(token: _token);

  static final Object _token = Object();

  static PlatformChannelsDemoPlatform _instance =
      MethodChannelPlatformChannelsDemo();

  /// The default instance of [PlatformChannelsDemoPlatform] to use.
  ///
  /// Defaults to [MethodChannelPlatformChannelsDemo].
  static PlatformChannelsDemoPlatform get instance => _instance;

  /// Platform-specific implementations should set this with their own
  /// platform-specific class that extends [PlatformChannelsDemoPlatform] when
  /// they register themselves.
  static set instance(PlatformChannelsDemoPlatform instance) {
    PlatformInterface.verifyToken(instance, _token);
    _instance = instance;
  }

  Future<String?> getApplicationName(String? prefix) {
    throw UnimplementedError('getApplicationName() has not been implemented.');
  }
}
