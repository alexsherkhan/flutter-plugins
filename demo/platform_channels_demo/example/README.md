# Platform Channels demo example

Demonstrates how to use the platform_channels_demo plugin.

## Usage

Get dependency

```shell
flutter-aurora pub get
```

Build aurora application

```shell
flutter-aurora build aurora --release
```
