# platform_channels_demo

An example of a platform-dependent plugin that works with the Aurora OS via the [Platform Channels](https://docs.flutter.dev/platform-integration/platform-channels).

![preview](data/preview.png)

Detailed information about the plugin can be found in the documentation:

[![button_docs](../../data/button_docs.png)](https://omprussia.gitlab.io/flutter/flutter/examples/platform-channels/)

