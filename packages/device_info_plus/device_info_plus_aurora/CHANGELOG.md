## Updated: 04/24/2024 13:28:57 PM

## Info

- Last tag: device_info_plus_aurora-0.5.0
- Released: 2

## Versions

- Version: device_info_plus_aurora-0.5.0 (22/04/2024)
- Version: device_info_plus_aurora-0.0.1 (28/12/2023)

### Version: device_info_plus_aurora-0.5.0 (22/04/2024)

#### Feature

- Add example for device_info_plus_aurora, fix AuroraOS 5.
[xdga_directories][bug] Check for duplicates in xdga_directories.
[change] Switch to a more recent changeln.

### Version: device_info_plus_aurora-0.0.1 (28/12/2023)

#### Feature

- Add changeln, update dependency ref, readme.
