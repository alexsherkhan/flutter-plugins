## Updated: 04/24/2024 13:29:02 PM

## Info

- Last tag: sensors_plus_aurora-0.5.0
- Released: 2

## Versions

- Version: sensors_plus_aurora-0.5.0 (22/04/2024)
- Version: sensors_plus_aurora-0.0.1 (28/12/2023)

### Version: sensors_plus_aurora-0.5.0 (22/04/2024)

#### Change

- Use flutter interface client_wrapper.
[package_info_plus_aurora][change] Use flutter interface client_wrapper.
[flutter_keyboard_visibility_aurora][change] Use flutter interface client_wrapper.
[camera_aurora][change] Use flutter interface client_wrapper.
[sqflite_aurora][change] Use flutter interface client_wrapper.
[flutter_secure_storage_aurora][feature] Add example.
[flutter_local_notifications_aurora][feature] Add example.
[cached_network_image][feature] Add example for cached_network_image.
[flutter_cache_manager][feature] Add example for flutter_cache_manager.
[sqflite_aurora][feature] Add example for sqflite_aurora.
[camera_aurora][feature] Add example for camera_aurora.
[device_info_plus_aurora][feature] Add example for device_info_plus_aurora, fix AuroraOS 5.
[xdga_directories][bug] Check for duplicates in xdga_directories.
[change] Switch to a more recent changeln.

#### Feature

- Add example application.

### Version: sensors_plus_aurora-0.0.1 (28/12/2023)

#### Feature

- Add changeln, update dependency ref, readme.
