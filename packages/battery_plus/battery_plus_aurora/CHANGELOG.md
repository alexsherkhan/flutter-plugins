## Updated: 04/24/2024 13:28:55 PM

## Info

- Last tag: battery_plus_aurora-0.5.0
- Released: 2

## Versions

- Version: battery_plus_aurora-0.5.0 (22/04/2024)
- Version: battery_plus_aurora-0.0.1 (28/12/2023)

### Version: battery_plus_aurora-0.5.0 (22/04/2024)

#### Feature

- Add example application.

### Version: battery_plus_aurora-0.0.1 (28/12/2023)

#### Feature

- Add changeln, update dependency ref, readme.
