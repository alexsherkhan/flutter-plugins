## Updated: 04/24/2024 13:28:55 PM

## Info

- Last tag: cached_network_image-0.5.0
- Released: 1

## Versions

- Version: cached_network_image-0.5.0 (22/04/2024)

### Version: cached_network_image-0.5.0 (22/04/2024)

#### Feature

- Add example for cached_network_image.
[flutter_cache_manager][feature] Add example for flutter_cache_manager.
[sqflite_aurora][feature] Add example for sqflite_aurora.
[camera_aurora][feature] Add example for camera_aurora.
[device_info_plus_aurora][feature] Add example for device_info_plus_aurora, fix AuroraOS 5.
[xdga_directories][bug] Check for duplicates in xdga_directories.
[change] Switch to a more recent changeln.
