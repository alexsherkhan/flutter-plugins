// SPDX-FileCopyrightText: Copyright 2024 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause
import 'package:flutter/material.dart';
import 'package:internal_aurora/abb_bar_action.dart';
import 'package:internal_aurora/list_button.dart';
import 'package:internal_aurora/list_item_data.dart';
import 'package:internal_aurora/list_item_info.dart';
import 'package:internal_aurora/list_separated.dart';
import 'package:internal_aurora/theme/colors.dart';
import 'package:internal_aurora/theme/theme.dart';

import 'plugin_impl.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({super.key});

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  // All functionality is included in the *impl class
  // and the methods have a description.
  final PluginImpl _impl = PluginImpl();

  // Future data with for demonstration refresh logic
  late Future<int> _randomNumber;

  @override
  void initState() {
    _init();
    super.initState();
  }

  /// The method should not change its name for standardization
  Future<void> _init() async {
    if (!mounted) return;
    setState(() {
      _randomNumber = _impl.getNumber();
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      // Common theme for examples
      theme: internalTheme,
      home: Scaffold(
        appBar: AppBar(
          // Title
          title: const Text('Internal'),
          // Add button for refresh data.
          actions: [AppBarAction(onPressed: _init)],
        ),
        // Custom list for widgets with separated.
        body: ListSeparated(
          children: [
            // The plugin must have a description in the example.
            const ListItemInfo("""
            This is an example implementation of the example for the plugin.
            """),

            // We have a data block in which we can display
            // information in the formats: stream, future, value.
            ListItemData(
              'Check Aurora OS',
              InternalColors.purple,
              description: 'Displays whether the current system is Aurora OS',
              widthData: 140,
              stream: _impl.onIsAurora(),
              builder: (value) => value?.toString().toUpperCase(),
            ),

            // Custom widget in ListItemData
            ListItemData('Check Aurora OS', InternalColors.orange,
                description: """
                Displays whether the current system is Aurora OS
                with custom widget
                """,
                widthData: 140,
                stream: _impl.onIsAurora(), builder: (value) {
                  final boxDecoration = BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(30.0),
                  );
                  switch (value) {
                    case true:
                      return Container(
                        decoration: boxDecoration,
                        padding: const EdgeInsets.all(8),
                        child: const Icon(
                          Icons.verified,
                          color: Colors.green,
                        ),
                      );
                    case false:
                      return Container(
                        decoration: boxDecoration,
                        padding: const EdgeInsets.all(8),
                        child: const Icon(
                          Icons.new_releases,
                          color: Colors.red,
                        ),
                      );
                    default:
                      return Container(
                        decoration: boxDecoration,
                        padding: const EdgeInsets.all(8),
                        child: const Icon(
                          Icons.hourglass_bottom,
                          color: Colors.blueGrey,
                        ),
                      );
                  }
                }),

            // We will demonstrate how the refresh feature works
            ListItemData(
              'Random number',
              InternalColors.coal,
              description: 'This random number should be updated after refresh',
              // If you do not specify the size, the data block will be displayed as a list
              widthData: null,
              future: _randomNumber,
              builder: (value) => value?.toString().toUpperCase(),
            ),

            // Button in common style for example
            ListButton('Refresh', InternalColors.green, onPressed: _init)
          ],
        ),
      ),
    );
  }
}
