## Updated: 04/24/2024 13:29:03 PM

## Info

- Last tag: sqflite_aurora-0.5.0
- Released: 2

## Versions

- Version: sqflite_aurora-0.5.0 (22/04/2024)
- Version: sqflite_aurora-0.0.1 (28/12/2023)

### Version: sqflite_aurora-0.5.0 (22/04/2024)

#### Bug

- Fix sqflite type int.

#### Change

- Use flutter interface client_wrapper.
[flutter_secure_storage_aurora][feature] Add example.
[flutter_local_notifications_aurora][feature] Add example.
[cached_network_image][feature] Add example for cached_network_image.
[flutter_cache_manager][feature] Add example for flutter_cache_manager.

#### Feature

- Add example for sqflite_aurora.
[camera_aurora][feature] Add example for camera_aurora.
[device_info_plus_aurora][feature] Add example for device_info_plus_aurora, fix AuroraOS 5.
[xdga_directories][bug] Check for duplicates in xdga_directories.
[change] Switch to a more recent changeln.

### Version: sqflite_aurora-0.0.1 (28/12/2023)

#### Feature

- Add changeln, update dependency ref, readme.
